## Introduction

This library is a simple implementation of Streams from the [dart:async library](https://api.flutter.dev/flutter/dart-async/dart-async-library.html).

Currently only the [StreamController](https://api.flutter.dev/flutter/dart-async/StreamController-class.html), [Stream](https://api.flutter.dev/flutter/dart-async/Stream-class.html) and [StreamSubscription](https://api.flutter.dev/flutter/dart-async/StreamSubscription-class.html) are implemented in a synchronous manner.

## Usage

```cpp
#include <iostream>
#include <sstream>
#include <string>

#include "stream.hpp"

using namespace std;
using namespace stream;

void main() {
  stream::StreamController<string> controller;

  stream::Listener<string> data = [](string e) {
    cout << "In data: " << e << endl;
  };
  stream::Listener<> done = []() {
    cout << "In done" << endl;
  };

  auto& stream = controller.getStream();
  stream.listen(data1, done1);

  controller.add("message 1");
  controller.add("message 2");

  controller.close();

  return 0;
}
```