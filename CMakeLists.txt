cmake_minimum_required(VERSION 3.16)
project(stream)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_compile_definitions(
  LIB_STREAM_VERSION="1.0.0"
)

set(EXECUTABLE_NAME main)


if(CMAKE_COMPILER_IS_GNUCC)
  add_compile_options(
    -Wall -Wextra -Wconversion -Woverloaded-virtual -Wpedantic
  )
endif(CMAKE_COMPILER_IS_GNUCC)

add_subdirectory(src)
