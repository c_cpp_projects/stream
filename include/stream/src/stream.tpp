#ifndef STREAM_SRC_STREAM_TPP
#define STREAM_SRC_STREAM_TPP

#include <cassert>
#include <memory>
#include <vector>

#include "aliases.tpp"
#include "stream_subscription.tpp"

namespace stream {

  template<typename T>
  class StreamController;

  template<typename T>
  class Stream {
   private:
    friend class StreamController<T>;
    friend class StreamSubscription<T>;

    using StreamSubscriptionPtr = StreamSubscription<T>*;

    using StreamSubscriptionContainer = std::vector<StreamSubscriptionPtr>;
    using StreamSubscriptionContainerPtr = std::shared_ptr<StreamSubscriptionContainer>;

   private:
    StreamSubscriptionContainerPtr m_pSubscriptions;
    size_t m_subsAmount;

   private:
    Stream() noexcept : m_pSubscriptions(nullptr), m_subsAmount(0) {}

    inline void m_forEachSub(std::function<void(StreamSubscriptionPtr)> forEach) {
      assert(nullptr != forEach);

      auto amount = m_subsAmount;
      auto subscriptions = m_pSubscriptions;

      for (size_t i = 0; i < amount; ++i) {
        forEach(subscriptions->at(i));
      }
    }

    // Add an event of type T to all m_pSubscriptions.
    void m_addEvent(T&& event) noexcept {
      if (0 == m_subsAmount) return;

      m_forEachSub([&event](StreamSubscriptionPtr sub) {
        sub->m_handleData(std::forward<T>(event));
      });
    }

    void m_close() noexcept {
      if (0 == m_subsAmount) return;

      auto subscriptions = m_pSubscriptions;
      for (size_t i = 0; i < m_subsAmount; ++i) {
        subscriptions->at(i)->m_handleDone();
      }

      m_dispose();
    }

    void m_dispose() noexcept {
      if (nullptr == m_pSubscriptions) return;
      if (0 == m_subsAmount) {
        m_pSubscriptions = nullptr;
        return;
      }

      auto subscriptions = m_pSubscriptions;
      for (size_t i = 0; i < m_subsAmount; ++i) {
        delete subscriptions->at(i);
      }

      m_pSubscriptions = nullptr;
      m_subsAmount = 0;
    }

   public:
    Stream(const Stream<T>&) = delete;

    Stream(Stream<T>&& source) noexcept :
        m_pSubscriptions(std::move(source.m_pSubscriptions)),
        m_subsAmount(std::move(source.m_subsAmount)) {
      source.m_subsAmount = 0;
    }

    virtual ~Stream() noexcept {
      m_dispose();
    }

    Stream<T>& operator=(const Stream<T>&) = delete;

    Stream<T>& operator=(Stream<T>&& source) noexcept {
      m_pSubscriptions = std::move(source.m_pSubscriptions);
      m_subsAmount = std::move(source.m_subsAmount);

      source.m_pSubscriptions = nullptr;
      source.m_subsAmount = 0;
    }

    /**
    * @brief Adds a @c StreamSubscription to this @c Stream . 
    * 
    *        Returns a @c StreamSubscription which handles events from this
    *        @c Stream using the provided onData, onError and onDone handlers.
    *        The handlers can be changed on the @c StreamSubscription , but they
    *        start out as the provided functions.
    * 
    *        On each data event from this @c Stream, the subscriber's onData handler
    *        is called. If onData is null, nothing happens.
    * 
    * @return The @c StreamSubscription that was added to this @c Stream .
    */
    StreamSubscription<T>& listen(const Listener<T> onData = nullptr,
                                  const Listener<> onDone = nullptr) noexcept {
      if (0 == m_subsAmount) {
        auto sub = new StreamSubscription<T>(this);

        m_pSubscriptions = std::make_shared<StreamSubscriptionContainer>();
        m_pSubscriptions->push_back(sub);
        m_subsAmount = 1;

        sub->onData(onData);
        sub->onDone(onDone);

        return *sub;
      }

      if (m_subsAmount >= m_pSubscriptions->size()) {
        // double size
        auto newSubscriptions =
          std::make_shared<StreamSubscriptionContainer>(m_subsAmount * 2);

        size_t i = 0;

        // Copy old subs
        for (size_t j = 0; j < m_subsAmount; ++j) {
          // Dont copy if sub is cancelled
          if (m_pSubscriptions->at(j)->isCancelled()) continue;

          newSubscriptions->at(i) = m_pSubscriptions->at(j);
          ++i;
        }

        // Initialize spots for new subs with nullptr
        for (; i < newSubscriptions->size(); ++i) {
          newSubscriptions->at(i) = nullptr;
        }

        m_pSubscriptions = newSubscriptions;
      }

      auto sub = new StreamSubscription<T>(this);

      m_pSubscriptions->at(m_subsAmount) = sub;
      ++m_subsAmount;

      sub->onData(onData);
      sub->onDone(onDone);

      return *sub;
    }
  };

}  // namespace stream

#endif  // STREAM_SRC_STREAM_TPP