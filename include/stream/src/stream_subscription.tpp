#ifndef STREAM_SRC_STREAM_SUBSCRIPTION_TPP
#define STREAM_SRC_STREAM_SUBSCRIPTION_TPP

#include "aliases.tpp"

namespace stream {

  template<typename T>
  class Stream;

  template<typename T>
  class StreamSubscription {
   private:
    friend class Stream<T>;

   private:
    const Stream<T>* m_pPublisher;
    bool m_isPaused;
    Listener<T> m_onData;
    Listener<> m_onDone;

   private:
    StreamSubscription(const Stream<T>* publisher) noexcept :
        m_pPublisher(publisher),
        m_isPaused(false),
        m_onData(nullptr),
        m_onDone(nullptr) {}

    void m_handleData(T&& data) const noexcept {
      if (nullptr == m_onData) return;
      if (isCancelled()) return;
      if (isPaused()) return;

      m_onData(std::forward<T>(data));
    }

    void m_handleDone() const noexcept {
      if (nullptr == m_onDone) return;
      if (isCancelled()) return;

      m_onDone();
    }

    void m_dispose() noexcept {
      m_pPublisher = nullptr;
      m_onData = nullptr;
      m_onDone = nullptr;
    }

   public:
    StreamSubscription(const StreamSubscription<T>&) = delete;
    StreamSubscription(StreamSubscription<T>&&) = delete;

    virtual ~StreamSubscription() noexcept {
      m_dispose();
    }

    friend bool operator==(const StreamSubscription<T>& l,
                           const StreamSubscription<T>& r) noexcept {
      // if not same address return.
      if (&l != &r) return false;

      if (l.m_pPublisher != r.m_pPublisher) return false;

      return true;
    }

    friend bool operator!=(const StreamSubscription<T>& l,
                           const StreamSubscription<T>& r) noexcept {
      return !(l == r);
    }

    StreamSubscription<T>& operator=(const StreamSubscription<T>&) = delete;
    StreamSubscription<T>& operator=(StreamSubscription<T>&&) = delete;

    /**
    * @brief Whether this @c StreamSubscription is cancelled.
    *  
    * @return @b False if this @c StreamSubscription is currently active and can receive
    *         events, @b true if @c StreamSubscription.cancel() has been called previously.
    */
    bool isCancelled() const noexcept {
      return nullptr == m_pPublisher;
    }

    /**
    * @brief Whether this @c StreamSubscription is currently paused.
    * 
    * @return @b False if this @c StreamSubscription can currently receive events
    *         and @b true if @c StreamSubscription.pause() has been called previously.
    */
    bool isPaused() const noexcept {
      return m_isPaused;
    }

    /**
    * @brief Replaces the data event handler of this @c StreamSubscription .
    * 
    *        The handleData function is called for each data event of the
    *        @c Stream after this function is called. If handleData is null,
    *        data events are ignored.
    * 
    *        This method replaces the current handler set by the invocation
    *        of @c Stream.listen or by a previous call to onData.
    * 
    * @param handleOnData 
    */
    void onData(const Listener<T> handleOnData) noexcept {
      m_onData = handleOnData;
    }

    /**
    * @brief Replaces the done event handler of this @c StreamSubscription .
    * 
    *        The handleDone function is called when the @c Stream closes.
    *        The value may be null, in which case no function is called. 
    *
    *        This method replaces the current handler set by the invocation
    *        of @c Stream.listen , by calling asFuture, or by a previous call
    *        to onDone.
    * 
    * @param handleOnDone The function that is called when the @c Stream closes.
    */
    void onDone(const Listener<> handleOnDone) noexcept {
      m_onDone = handleOnDone;
    }

    /**
   * @brief Cancels this @c StreamSubscription. 
   *        After this call, this @c StreamSubscription no longer receives events.
   * 
   */
    void cancel() noexcept {
      if (isCancelled()) return;
      m_dispose();
    }

    /**
    * @brief Pause this @c StreamSubscription until further notice.
    *        Any events emitted from the @c Stream while this @c StreamSubscription
    *        is paused are ignored.
    */
    void pause() noexcept {
      m_isPaused = true;
    }

    /**
    * @brief Resumes after a pause.
    */
    void resume() noexcept {
      m_isPaused = false;
    }
  };

}  // namespace stream

#endif  // STREAM_SRC_STREAM_SUBSCRIPTION_TPP