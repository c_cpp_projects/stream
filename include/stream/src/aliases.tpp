#ifndef STREAM_SRC_ALIASES_TPP
#define STREAM_SRC_ALIASES_TPP

#include <functional>

namespace stream {

  template<typename... Args>
  using Listener = std::function<void(Args...)>;

}  // namespace stream

#endif  // STREAM_SRC_ALIASES_TPP