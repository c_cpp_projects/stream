#ifndef STREAM_SRC_STREAM_CONTROLLER_TPP
#define STREAM_SRC_STREAM_CONTROLLER_TPP

#include <memory>

#include "stream.tpp"

namespace stream {

  template<typename T>
  class StreamController {
   private:
    using StreamPtr = std::unique_ptr<Stream<T>>;

   private:
    StreamPtr m_pStream;

   private:
    void m_dispose() noexcept {
      if (isClosed()) return;

      m_pStream->m_close();
      m_pStream->m_dispose();
      m_pStream = nullptr;
    }

   public:
    StreamController() noexcept {
      m_pStream = StreamPtr(new Stream<T>());
    }

    StreamController(const StreamController&) = delete;

    StreamController(StreamController&& source) noexcept :
        m_pStream(std::move(source.m_pStream)) {}

    virtual ~StreamController() noexcept {
      m_dispose();
    }

    StreamController<T>& operator=(const StreamController<T>&) = delete;

    StreamController<T>& operator=(StreamController<T>&& source) noexcept {
      m_pStream = std::move(source.m_pStream);
    }

    /**
    * @brief Whether there is a @c StreamSubscription on the @c Stream .
    * 
    * @return @b False if the @c Stream this @c StreamController is controlling
    *         has no subscribers or if this controller is closed, else @b true .
    */
    bool hasListeners() const noexcept {
      if (isClosed()) return false;
      return m_pStream->m_subsAmount > 0;
    }

    /**
    * @brief Whether this @c StreamController is closed for adding more events.
    * 
    *        The @c StreamController becomes closed by calling the close method.
    *        New events cannot be added, by calling add, to a closed controller.
    * 
    * @return @b True if @c StreamController.close() has been called, else @b false.
    */
    bool isClosed() const noexcept {
      return nullptr == m_pStream;
    }

    /**
    * @brief The @c Stream that this @c StreamController is controlling. 
    * 
    *        Throws a @c std::logic_error if this @c StreamController
    *        has been closed. 
    */
    Stream<T>& getStream() const noexcept(false) {
      if (isClosed()) throw std::logic_error("Stream has already been closed.");

      return *m_pStream;
    }

    /**
    * @brief Sends a data event.
    * 
    *        Throws a @c std::logic_error if this @c StreamController
    *        has been closed. 
    */
    void add(T&& event) noexcept(false) {
      if (isClosed())
        throw std::logic_error("Cannot add event after closing StreamController.");

      m_pStream->m_addEvent(std::forward<T>(event));
    }

    /**
    * @brief Closes the @c Stream . 
    *        
    *        No further events can be added to a closed @c Stream .
    */
    void close() noexcept {
      m_dispose();
    }
  };

}  // namespace stream

#endif  // STREAM_SRC_STREAM_CONTROLLER_TPP