#include <iostream>
#include <sstream>
#include <string>

#include "stream.hpp"

using namespace std;
using namespace stream;

class Person {
 private:
  int m_id;
  string m_name;

 public:
  Person(int id, string name) noexcept : m_id(id), m_name(name) {
    cout << "Constructor " << toString() << '\n';
  }

  Person(const Person& source) noexcept : m_id(source.m_id), m_name(source.m_name) {
    cout << "Copy Constructor " << source.toString() << '\n';
  }

  Person(Person&& source) noexcept :
      m_id(std::move(source.m_id)), m_name(std::move(source.m_name)) {
    source.m_id = 0;
    source.m_name.clear();

    cout << "Move Constructor " << toString() << '\n';
  }

  virtual ~Person() noexcept {
    cout << "Destructor " << toString() << '\n';
  }

  friend ostream& operator<<(ostream& cout, const Person& source) noexcept {
    cout << source.toString();
    return cout;
  }

  Person& operator=(const Person& source) {
    m_id = source.m_id;
    m_name = source.m_name;

    cout << "Copy Assignment " << source.toString() << '\n';

    return *this;
  }

  Person& operator=(Person&& source) {
    m_id = std::move(source.m_id);
    m_name = std::move(source.m_name);

    source.m_id = 0;
    source.m_name.clear();

    cout << "Move Assignment " << toString() << '\n';

    return *this;
  }

  string toString() const noexcept {
    std::stringstream s;

    s << "Person{";
    s << "id: " << m_id << ", ";
    s << "name: " << m_name;
    s << '}';

    return s.str();
  }
};

void testString();
void testPerson();

int main() {
#ifdef LIB_STREAM_VERSION
  cout << "LIB_STREAM_VERSION " << LIB_STREAM_VERSION << endl;
#endif

  testString();
  // testPerson();

  return 0;
}

void testString() {
  StreamController<string> controller;

  Listener<string> data1 = [](string e) {
    cout << "In data1: " << e << endl;
  };
  Listener<> done1 = []() {
    cout << "In done1" << endl;
  };
  Listener<string> data2 = [](string e) {
    cout << "In data2: " << e << endl;
  };
  Listener<> done2 = []() {
    cout << "In done2" << endl;
  };
  Listener<string> data3 = [](string e) {
    cout << "In data3: " << e << endl;
  };
  Listener<> done3 = []() {
    cout << "In done3" << endl;
  };

  auto& stream = controller.getStream();
  auto& sub1 = stream.listen(data1, done1);
  auto& sub2 = stream.listen(data2, done2);
  stream.listen(data3, done3);

  controller.add("nonaned 1");

  sub1.pause();
  cout << '\n';
  controller.add("alala 2");

  sub1.resume();
  cout << '\n';
  controller.add("nix 3");

  sub2.cancel();
  cout << '\n';
  controller.add("da 3");

  cout << '\n';
  controller.close();

  cout << '\n';
  try {
    controller.add("da 3");
  } catch (const std::logic_error& e) { std::cerr << e.what() << '\n'; }
}

void testPerson() {
  Person p1{1, "herbert"};
  // Person p2{88, "peter"};
  // Person p3{987, "srar"};

  // StreamController<Person> controller;
  // controller.getStream().listen([](Person p) {
  //   cout << "copy L1: " << p << '\n';
  // });
  StreamController<Person&> controller;
  controller.getStream().listen([](Person& p) {
    cout << "ref L1: " << p << '\n';
  });

  // controller.add({88, "peter"});
  // controller.add(std::move(p1));
  controller.add(p1);

  controller.close();
}
